/*
�������� ���������, ������� ����������� ������� ����� � ������� ��:��:��,
� ����� ������� ����������� � ����������� �� ���������� ������� ("������ ����", "������ ����" � �.�.)
*/

#define _CRT_SECURE_NO_WARNINGS

// ���������� ������� ��� ������ ���������
#define MES_INPUT "Vvedite tekushhee vremja v formate hh:mm:ss\n"
#define MES_ERROR "\n!---------------------------------!\n! Proshu Vas, bud'te vnimatel'ny! !\n!---------------------------------!\n"
#define MES_MOR "Dobroe utro!\n"
#define MES_DAY "Dobryj den'!\n"
#define MES_EVN "Dobryj vecher!\n"
#define MES_NGH "Dobroj nochi!\n"

#include <stdio.h>

int main()
{
	int hour, minute, second;
	int format;
	
	//����������� ������� ����� � ��������� �������� ������
	printf(MES_INPUT);
	do {
		fflush(stdin);
		format = scanf("%d%*c%d%*c%d", &hour, &minute, &second);
		if (format == 3 && hour >= 0 && hour <= 24 && minute >= 0 && minute <= 60 && second>=0 && second <= 60)
		{

			if (hour == 24)
				hour = 0;
			if (minute == 60)
				minute = 0;
			if (second == 60)
				second = 0;
			break;
		}
		else
			printf(MES_ERROR);
			printf("\n" MES_INPUT);
		
	} while (format != 3 || hour < 0 || hour > 24 || minute < 0 || minute > 60 || second < 0 || second > 60);

	//������� ������������ ����� ����� �� ����
	printf("\nTekushhee vremja = %2.2d:%2.2d:%2.2d - ", hour, minute, second);

	//������� ��������� � ���������� �� ������� �����
	if (hour>=6 && hour<12)
		printf(MES_MOR);
	else if(hour>=12 && hour<18)
		printf(MES_DAY);
	else if(hour>=18 && hour<24)
		printf(MES_EVN);
	else 
		printf(MES_NGH);

	printf("\n");

	return 0;
}