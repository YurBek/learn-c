/*
1.4. �������� ���������, ������� ��������� ���� �� ������������ ������� (����, �����)
� ����������� (����������). ������ �������� � ���� ���� ����� �����,
��������� � ���� ������������� ����� � ��������� �� 1 �����.
1 ��� = 12 ������. 1 ���� = 2.54 ��.
*/

#define _CRT_SECURE_NO_WARNINGS

// ���������� ������� ��� ������ ���������
#define INPUT_MES "Vvedite Vash rost v futah (foot) i djujmah (inch) po formatu: 5f6i\n"
#define OUT_MES_1 "\nVash rost %d foot %d inch"
#define OUT_MES_2 " sootvetsvuet %5.1f cm\n"
#define MES_ERROR "\n!---------------------------------!\n! Proshu Vas, bud'te vnimatel'ny! !\n!---------------------------------!\n"

#include <stdio.h>

int main()
{
	int foot, inch, format;
	char charFoot, charInch;
	float rost;
	
	// ����������� ���� � ��������� �������� ������
	printf(INPUT_MES);
	do{
		fflush(stdin);
		format = scanf("%d%c%d%c", &foot, &charFoot, &inch, &charInch);
		if (format == 4 && foot >= 0 && inch >= 0 && (charFoot == 'F' || charFoot == 'f') && (charInch == 'I' || charInch == 'i'))
			break;
		else
			printf(MES_ERROR);
			printf("\n" INPUT_MES);
	} while (format != 4 || foot < 0 || inch < 0 || charFoot != 'F' || charFoot != 'f' || charInch != 'I' || charInch != 'i');
	
	// ��������� ���� � �����, � ����� � ����������
	printf(OUT_MES_1, foot, inch);
	rost = 2.54*((12. * foot) + inch);
	printf(OUT_MES_2, rost);

	return 0;
}