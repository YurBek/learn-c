/*
1.3. �������� ���������, ������� ��������� �������� ���� �� �������� � �������,
� ��������, ������������ �� ������� ��� �����.
��������: 45.00D - �������� �������� � ��������, � 45.00R - � ��������.
���� ������ �������������� �� ������� %f%c
*/

#define _CRT_SECURE_NO_WARNINGS

// ���������� ������� ��� ������ ���������
#define INPUT_MES "Vvedite velichinu ugla.\nEsli ugol v gradusah - ispol'zujte ""D"" ili ""d""\nEsli ugol v radianah - ispol'zujte ""R"" ili ""r""\nNaprimer, 45D, ili 45d, ili 45R, ili 45r\n"
#define OUT_MES_11 "\nVash ugol = %6.2f degrees"
#define OUT_MES_12 " sootvetsvuet = %6.2f radians\n"
#define OUT_MES_21 "\nVash ugol = %6.2f radians"
#define OUT_MES_22 " sootvetsvuet = %6.2f degrees\n"
#define MES_ERROR "\n!---------------------------------!\n! Proshu Vas, bud'te vnimatel'ny! !\n!---------------------------------!\n"

#include <stdio.h>
#include <math.h>

int main()
{
	float angle, pi = 3.14159;
	char size;
	int format;

	printf(INPUT_MES);
		
	// ����������� ���� � �����������, ��������� �������� ������
	do {
		fflush(stdin);
		format = scanf("%f%c", &angle, &size);
		if (format == 2 && ((size == 'D' || size == 'd') || (size == 'R' || size == 'r')))
				break;
		else
			printf(MES_ERROR);
		printf("\n" INPUT_MES);

	} while (format != 2 || size != 'D' || size != 'd' || size != 'R' || size != 'r');
	
	if (size == 'D' || size == 'd')
	{
		// ����������� ������� � �������
		printf(OUT_MES_11, angle);
		// ���� �������� ���� 360 �������� � ������
		if (angle > 360)
		{
			angle = angle - (truncf(angle / 360.f)*360.f);
			printf(" (ili = %6.2f degrees)", angle);
		}
		angle=angle*pi/180.f;
		printf(OUT_MES_12, angle);
	}
	else 
	{
		// ����������� ������� � �������
		printf(OUT_MES_21, angle);
		// ���� �������� ���� 2PI ������ � ������
		if (angle > 2*pi)
		{
			angle = angle - (truncf(angle / (2 * pi))*(2 * pi));
			printf(" (ili = %6.2f radians)", angle);
		}
		angle=angle * 180.f/ pi;
		printf(OUT_MES_22, angle);
	}

	return 0;
}