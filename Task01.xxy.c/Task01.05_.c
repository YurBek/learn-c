/*
1.5. �������� ���������, ������� ��������� ������ �� ������������ �
������� � �� �����, ���������� �� ������.
*/

#define _CRT_SECURE_NO_WARNINGS

// ��������� ������ ������ ������ 80 ��������
#define WIDTH_SCREEN 80

#include <stdio.h>
#include <string.h>

int main()
{
	char str[WIDTH_SCREEN + 1];
	int i, pos;
	
// ����������� ������
	printf("Vvedite proizvol'nuju stroku.\n");
	printf("Mozhno ispol'zovat' ljubye simvoly, cifry i probely!..\n\n");
	gets(str);
	
// ���������� "���������" �������� ������
	printf("\n");
	for (i = 1; i <= WIDTH_SCREEN; i++)
		if (i%10==0)
			printf("%d", i/10);
		else printf("%c", '.');
	
// ������� ������ �� ������ ������ - ������ 1 (����� �������!!!)
	printf("var.1\n");
	pos = (WIDTH_SCREEN - strlen(str)) / 2 + strlen(str);
	printf("%*s\n", pos, str);

// ��� ��� ������
	printf("var.1 short\n%*s\n", ((WIDTH_SCREEN - strlen(str)) / 2 + strlen(str)), str);

// ������� ������ �� ������ ������ - ������ 2 (���������� ����)
	printf("var.2\n");
	for (i = 1; i <= (WIDTH_SCREEN - strlen(str)) / 2; i++)
		printf("%c", ' ');
	puts(str);
	printf("\n");

/*
������� ������ �� ������ ������ - ������ 3
��������� sprintf � ���. ������ (buff) ������� ����������� ������������������
��. ���� %Ns, ��� N - ��� ����� ������ ����������� ������ ���� ������ � ��������
� ������ ��������� ������. ����������� ������������������ ���������� � printf.
*/
	
	char buff[10];
	
	sprintf(buff, "%%%ds", pos);

// �������� ����� ����������� ������������������ � ��� ����������
	printf("var.3 e.s.: %s\n", buff);

	printf(buff, str);
	printf("\n");
	
	return 0;
}