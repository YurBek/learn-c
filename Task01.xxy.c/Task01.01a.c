/*
1.1. �������� ���������, ������� ����������� � ������������ ���, ���� � ���,
� ����� ����������� ����������� ����� � ����,
������� ������������ � ���������� ��������� (��������, ����������, �����).
*/

/*
���� ������ ����� ���� (���) - ������ �����, ����� ������ �� ������� ��� ���������� ����.
������ �������������� ��� �������� ������ � ������ �� 20 �� 65 ��� �� �������:
����� ���� � �� (�) ��������� �� ���� � ������ (�) � �������� �/(�*�)
��������: ��� 65 ��, ���� 170 ��. ������, 65 / (1.7 * 1.7) = 22.5
��� ������ ��� � ����� - 19-25.
��� ������ ��� � ����� - 19-24.
*/

#define _CRT_SECURE_NO_WARNINGS

// ���������� ������� ��� ������ ���������
#define INPUT_MES "Ukazhite Vash "
#define INPUT_MES_11 "pol:\n"
#define INPUT_MES_12 " m - muzhskoj\n w - zhenskij\n"
#define INPUT_MES_21 "rost v santimetrah:\n"
#define INPUT_MES_31 "ves v kilogrammah:\n"
#define OUT_MES_1 "!      Prover'te svoi dannye!     !"
#define OUT_MES_21 "\n Pozdravljaem, "
#define OUT_MES_22 "\n K sozhaleniju "
#define OUT_MES_3 " Vasha massa tela "
#define OUT_MES_NORM "v norme!\n\n"
#define OUT_MES_INSUFFICIENT "nedostatochna...\n\n"
#define OUT_MES_EXCESSIVE "izbytochna...\n\n"
#define MES_HEAD_FOOT "!---------------------------------!"
#define WARNING_MES "! Proshu Vas, bud'te vnimatel'ny! !"

#include <stdio.h>

int main()
{
	const double indexMin = 19., indexMaleMax = 25., indexFemaleMax = 24.;
	double height = 0., weight = 0., bodyMassIndex = 0.;
	char inputChar = '\0';
	char sex[][7] = { "MALE", "FEMALE" };
		
	//����������� ��� (��� - m, ��� - w) � ��������� �������� ������
	printf(INPUT_MES INPUT_MES_11 INPUT_MES_12);
		
	do {
		fflush(stdin);
		inputChar = getchar();
		switch (inputChar)
		{
			case 'm':
				break;
			case 'w':
				break;
			default:
			{
				printf("\n" MES_HEAD_FOOT "\n" WARNING_MES "\n" MES_HEAD_FOOT "\n");
				printf("\n" INPUT_MES INPUT_MES_11 INPUT_MES_12);
			}
		}
	} while (inputChar != 'm' && inputChar != 'w');
	
	//����������� ���� (� ��) � ��������� �������� ������
	printf("\n" INPUT_MES INPUT_MES_21);
	do {
		fflush(stdin);
		scanf("%3lf[1234567890]", &height);
		if (height>0)
			break;
		else
		{
			printf("\n" MES_HEAD_FOOT "\n" WARNING_MES "\n" MES_HEAD_FOOT "\n");
			printf("\n" INPUT_MES INPUT_MES_21);
		}
	} while (height <= 0);
	height = height / 100.;
	
	//����������� ��� (� ��) � ��������� �������� ������
	printf("\n" INPUT_MES INPUT_MES_31);
	do {
		fflush(stdin);
		scanf("%3lf[1234567890]",&weight);
		if (weight>0)
			break;
		else
		{
			printf("\n" MES_HEAD_FOOT "\n" WARNING_MES "\n" MES_HEAD_FOOT "\n");
			printf("\n" INPUT_MES INPUT_MES_31);
		}
	} while (weight <= 0);
	
	//������� ������������ ��� �� ����
	printf("\n" MES_HEAD_FOOT "\n" OUT_MES_1 "\n" MES_HEAD_FOOT "\n");
	printf("   Vash pol  - %7s\n", sex[inputChar == 'm' ? 0 : 1]);
	printf("   Vash rost - %7.2lf m\n", height);
	printf("   Vash ves  - %7.2lf kg\n", weight);
	printf(MES_HEAD_FOOT "\n");

	//��������� ������ ����� ����
	bodyMassIndex = weight / (height*height);
	printf("  Vash indeks massy tela - %6.2lf\n", bodyMassIndex);
	printf(MES_HEAD_FOOT "\n");
	
	//���� ��� ������ ������ ������� - �������
	if (bodyMassIndex < indexMin)
		printf(OUT_MES_22 OUT_MES_3 OUT_MES_INSUFFICIENT);
	//���� ��� ������ ������� ������� ������� - �������
	else if (bodyMassIndex > indexFemaleMax && inputChar == 'w')
		printf(OUT_MES_22 OUT_MES_3 OUT_MES_EXCESSIVE);
	//���� ��� ������ ������� ������� ������� - �������
	else if (bodyMassIndex > indexMaleMax && inputChar == 'm')
		printf(OUT_MES_22 OUT_MES_3 OUT_MES_EXCESSIVE);
	//���� ��� � �������� - ��!!!
	else
		printf(OUT_MES_21 OUT_MES_3 OUT_MES_NORM);
	
	return 0;
}