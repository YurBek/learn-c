/*
4.3. �������� ���������, ������� ����������� ������ � ����������, �� ��������
�� ������ ����������� (��������� �������� � ����� ������� � ������ ������)
���������: ���� ������ - ��������� ��������� ��� �������� ������������ ������ � ���� ������.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

// ������� ������������ ���-�� �������� � ������
#define SYMBOL_MAX 79

int main()
{
	char string[SYMBOL_MAX + 1] = { '\0' };
	char *pTop, *pBot;
	
	int flag = 0;
	
	printf("Nazhmite \"Enter\",\n");//������� "Enter",
	printf("ili vvedite stroku kotoruju hotite proverit' na PALINDROM:\n");
	// ��� ������� ������ ������� ������ ��������� �� ���������:
	
	// ������ ������
	fgets(string, 80, stdin);
	if (string[strlen(string) - 1] == '\n')
		string[strlen(string) - 1] = '\0';

	//���� ���� ������� ������ �������
	if (strlen(string) == 0)
	{
		// ������ ������ �� ������������������ ��������� ��������
		srand((unsigned)time(NULL));

		pTop = string;
		pBot = string + (SYMBOL_MAX-1);
		while (pTop < pBot)
		{
			*pTop = *pBot = '0' + rand() % ('z' - '0' + 1); // ����� a-z � A-Z, ����� 0-9
			//*pTop = '0' + rand() % ('z' - '0' + 1); // ����� a-z � A-Z, ����� 0-9
			//*pBot = '0' + rand() % ('z' - '0' + 1); // ����� a-z � A-Z, ����� 0-9
			pTop++;
			pBot--;
		}
		*pTop = '0' + rand() % ('z' - '0' + 1); // ����� a-z � A-Z, ����� 0-9
		string[SYMBOL_MAX] = '\0';
		puts(string); // ������� ������
	}
	printf("\n");

	// ������� �����
	pTop = string;
	pBot = string + strlen(string) - 1;
	while (pTop<pBot)
	{
		if (*pTop != *pBot) // ������� ������ �����, ������ ������ ������
		{
			printf("%c != %c\n", *pTop, *pBot);
			printf("Vasha stroka != PALINDROM!\n"); // ���� ������ �� ���������!
			flag = 1;
			break;
		}
		printf("%c == %c\n", *pTop, *pBot);
		pTop++;
		pBot--;
	}

	if (flag != 1)
		printf("Otlichno! Vasha stroka == PALINDROM!!!\n"); // �������! ���� ������ ���������!!!

	return 0;
}