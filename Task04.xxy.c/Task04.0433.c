/*
4.4. �������� ��������� ��� ������ ����� ������� ������������������� � ������� �
�������������� ���������� ������ �������� ����������.
���������: ���������� ����� �������� ������ ���� �������� � �������������� ��������
���, ����� ������ � ������ ������������� ����� ���������.
*/

/*
�������� ������ 3.3. �������� ���������, ������� ��� �������� ������ ������� ����� �������
����� � ��� �����.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

// ������� ������������ ���-�� �������� � ������
#define SYMBOL_MAX 79

int main()
{
	char string[SYMBOL_MAX + 1] = { '\0' };
	char *pString = string;
	char *pWord = { NULL }, *pWordMax = {NULL};
	
	int i, inWord=0, counterWord=0, counterSymbol=0;
	
	// ������ ������
	printf("Nazhmite \"Enter\",\n");//������� "Enter",
	printf("ili vvedite proizvol'nuju stroku, razdeljaja slova probelami:\n");
	// ��� ������� ������������ ������, �������� ����� ���������:
	gets(pString);
	
	//���� ���� ������� ������ �������
	if (strlen(pString) == 0)
	{
		// ������ ������ �� ������������������ ��������� �������� � ��������
		srand((unsigned)time(NULL));

		for (i = 0; i<SYMBOL_MAX; i++)
		{
			switch (rand() % 3)
			{
			case 0:
				string[i] = ' '; // ������
				break;
			case 1:
				string[i] = 'a' + rand() % ('z' - 'a' + 1); // ������� a-z
				break;
			case 2:
				string[i] = 'A' + rand() % ('Z' - 'A' + 1); // ������� A-Z
				break;
			}
		}
		string[SYMBOL_MAX] = '\0';
		puts(pString); // ������� ������
	}
	printf("\n");

	// ������� �����
	while (*pString)
	{
		if (*pString != ' ' && inWord == 0) // ������� ������ �����, ������ ������ ������
		{
			inWord = 1;
			pWord = pString; // ������ ����� "�������" ����������
			putchar(*pString); // ������� ������ ������ �����
			counterWord++;
		}
		else if (*pString != ' ' && inWord == 1) // ���� ����� �������� ������ ������ �������
			putchar(*pString); // ������� ��������� ������� �����

		else if (*pString == ' ' && inWord == 1) // ������� ����� �����, ������ ������ ����� �� ������
		{
			inWord = 0;
			printf(" - %d\n", (pString - pWord)); // ������� ����� �����
			// ���������� ����� �������� ����� � ������ �����������
			if (counterSymbol < pString - pWord)
			{
				counterSymbol = pString - pWord;
				pWordMax = pWord;
			}
		}
		pString++;
	}
	// ���� ������ �� ����� � ����� �������
	if (*pString == '\0' && inWord == 1)
	{
		printf(" - %d\n", (pString - pWord));
		if (counterSymbol < pString - pWord)
		{
			counterSymbol = pString - pWord;
			pWordMax = pWord;
		}
	}
	
	if (counterWord != 0)
	{
		printf("\nVsego slov: %3d\n", counterWord); // ����� ����:
		printf("\nSamoe dlinnoe slovo v stroke:\n"); // ����� ������� ����� � ������:
		while (*pWordMax != ' ')
		{
			putchar(*pWordMax); // ������� ����������� ���� �����
			pWordMax++;
		}
		printf(" - %d\n", counterSymbol); // ������� ����� �����
	}
	else
	{
		printf("\nVsego slov: %3d\n", counterWord); // ����� ����
		printf("\nSozhaleem, v Vashej stroke slov ne obnaruzheno...\n"); // ���������, � ����� ������ ���� �� ����������...
	}


	return 0;
}