/*
4.5. �������� ���������, ����������� ������ (��. ������ 4.1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

// ������� ������������ ���-�� �������� � ������
#define SYMBOL_MAX 100
// ������� ������������ ���-�� �����
#define LINE_MAX 50

int main()
{
	FILE *fileIn, *fileOut;
	char fileNameIn[] = "textin_#.txt";
	char fileNameOut[] = "textout_#.txt";
	char *pFileNameIn = fileNameIn;
	char *pFileNameOut = fileNameOut;
	char string[LINE_MAX][SYMBOL_MAX + 1] = { '\0' };
	char *pString[LINE_MAX] = {NULL}, *temp=NULL;

	int i, j, flag=0, numberLine=0;
	
	// ��������� �� ������ ����� ������ ������
	printf("Vvedite nomer fajla (cifra ot 1 do 6): ");
	// ������� ����� ����� (����� �� 1 �� 6):
	fileNameIn[7] = fileNameOut[8] = getchar();

	// ��������� ���� �� ������
	if ((fileIn = fopen(pFileNameIn, "r")) == NULL)
		printf("Oshibka pri otkrytii fajla.\n");
		// ������ ��� �������� �����.

	// ��������� ���� �� ������
	if ((fileOut = fopen(pFileNameOut, "w")) == NULL)
		printf("Oshibka pri sozdanii fajla.\n");
		// ������ ��� �������� �����.
	
	// ������ ������ �� ����� � ���������� � ������ �����
	for (i = 0; i < LINE_MAX; i++)
	{
		if (feof(fileIn))
			break;
		else if (fgets(string[i], SYMBOL_MAX, fileIn))
		{
			// ������� \n � ����� ������
			j = strlen(string[i]) - 1;
			if (string[i][j] == '\n') string[i][j] = '\0';
			pString[i] = string[i]; // ��������� ��������� � ������� ������
		}
		numberLine++; // ������� ���������� ����������� �����
	}

	fclose(fileIn); // ��������� ���� �� ������
	printf("\n");

	// ���������� �������� �������� ��� ���������� ���������
	for (i = 0; i<numberLine-1; i++)
	{
		flag = 0; // ���� ��� �������� �������
		for (j = 0; j<(numberLine-1)-i; j++)
		{
			if (strlen(pString[j]) < strlen(pString[j + 1]))
			{
				temp = pString[j];
				pString[j] = pString[j+1];
				pString[j+1] = temp;
				flag = 1; // ���� ����� ��� ����=1
			}
		}
		if (flag == 0) // ���� ������ �� ���� ����=0 - ������� �� �����
			break;
	}
	
	// ������� �� ������ � ������� � ���� ��������������� ������
	for (i = 0; i<numberLine; i++)
	{
		//������� ������ ������ � ���� ������ �� �����
		printf("%3d - %s\n", strlen(pString[i]), pString[i]);
		//���������� ������ ������ � ���� ������ � ����
		fprintf(fileOut, "%3d - %s\n", strlen(pString[i]), pString[i]);
	}

	fclose(fileOut); // ��������� ���� �� ������

	printf("\nStroki prochitany iz fajla: %s\n", pFileNameIn);
	// ������ ��������� �� �����:
	printf("\nStroki  zapisany  v  fajl:  %s\n", pFileNameOut);
	// ������ �������� � ����:

	return 0;
}