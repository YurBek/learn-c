/*
4.2. �������� ���������, ������� � ������� ������� ���������� ������� ����� ������ � �������� �������.
���������: ������ �� ������� ��������� ������ ���������� �� char, � ������� ���������
������ ������ �������� ������� ����� (������������ - ������ ������� � ���������� ��������).
����� �� ���������� ����� ����� ������, ��������� ���� ������ �� ����������.
*/

/*
����� ������� � ������ �������� ����� ���� ������ ������. ���� ���� ����������� � ������ ������ ���������.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

// ������� ������������ ���-�� �������� � ������
#define SYMBOL_MAX 79
// ������� ������������ ���-�� ���� � ������
#define WORD_MAX 39

int main()
{
	char string[SYMBOL_MAX + 1] = { '\0' };
	char *pString = string;
	char *pWord[WORD_MAX] = { NULL };
	
	int i, j, inWord;
	
	printf("Nazhmite \"Enter\",\n");//������� "Enter",
	printf("ili vvedite proizvol'nuju stroku, razdeljaja slova probelami:\n");
	gets(string);

	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������ ������ �� ������������������ ��������� �������� � ��������
		srand((unsigned)time(NULL));

		for (i = 0; i<SYMBOL_MAX; i++)
		{
			switch (rand() % 3)
			{
			case 0:
				string[i] = ' '; // ������
				break;
			case 1:
				string[i] = 'a' + rand() % ('z' - 'a' + 1); // ������� a-z
				break;
			case 2:
				string[i] = 'A' + rand() % ('Z' - 'A' + 1); // ������� A-Z
				break;
			}
		}
		string[SYMBOL_MAX] = '\0';
		puts(string); // ������� ������
	}
	printf("\n");

	// ������� �����
	i = 0;
	j = 0;
	inWord = 0;
	while (*pString)
	{
		if (*pString != ' ' && inWord == 0) // ������� ������ �����, ������ ������ ������
		{
			inWord = 1;
			pWord[j++] = pString; // ������ ����� "�������" ����������
		}
		else if (*pString == ' ' && inWord == 1) // ������� ����� �����, ������ ������ ����� �� ������
		{
			inWord = 0;
			pWord[j++] = pString; // ������ ������� "�������" ����������
			//pSpace[j++] = pString; // ������ ������� "�������" ����������
		}
		pString++;
	}
	pWord[j++] = pString; // "�������" ���������� ������ '\0' � ����� ������
	
	// ������� ���������� "�����"
	/*i = 0;
	while (pWord[i]!=NULL)
	{
		printf("%s\n", pWord[i]);
		//printf("%s - d%\n", pString[i][0], pString[i][1]);
		i++;
	}
	printf("\n");*/

	// �������� � ����� ������� ����������
	i = 0;
	while (pWord[i] != NULL)
		i++;
	i = i - 2; // i++ ���� �� 1 ������, � ��� � ��� ������ '\0', ������� -2
	
	// ������� ����� � �������� �������
	printf("Slova v obratnom porjadke:\n"); // ����� � �������� �������:
	for ( ; i >-1;i--)
	{
		pString = pWord[i]; // �������� ������� ��������� �� ����� ��������� �� ������
		while (pString < pWord[i + 1]) // ���� ������� ��������� ������ ���������� �� ������� ��������� �� �����
			putchar(*pString++); // ������� ����������� �����
	}

	printf("\n");
	return 0;
}