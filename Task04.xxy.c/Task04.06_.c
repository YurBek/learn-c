/*
4.6. �������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������: ����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

// ������� ������������ ���-�� �������� � ����� ������������
#define SYMBOL_MAX 79
// ������� ������������ ���-�� �������������
#define LINE_MAX 25

int main()
{
	char name[LINE_MAX][SYMBOL_MAX + 1] = { '\0' };
	char *pYoung=NULL, *pOld=NULL;

	int numberRelation = 0, i = 0, flag=0, age = 0;
	int tempYoung = 0, tempOld = 0;
	
	printf("Vvedite kolichestvo rodstvennikov:\n");
	// ������� ���������� �������������:
	flag=0;
	do {
			fflush(stdin);
			flag = scanf("%d", &numberRelation); // ������ ����������
			if (flag == 1 && numberRelation>0)
				break;
			else
				printf("\n!---------------------------------!\n! Proshu Vas, bud'te vnimatel'ny! !\n!---------------------------------!\n");
			printf("Vvedite kolichestvo rodstvennikov:\n");
	} while (flag != 1 || numberRelation <= 0);

	printf("\nVvedite imena rodstvennikov i ih vozrast:\n");
	// ������� ����� ������������� � �� �������.
	for (i = 0; i<numberRelation; i++)
	{
		printf("%d -    imja: ", i+1); // - ���:
		fflush(stdin);
		fgets(name[i], 80, stdin); // ������ ���
		if (name[i][strlen(name[i]) - 1] == '\n')
			name[i][strlen(name[i]) - 1] = '\0';
		printf("%d - vozrast: ", i+1); // - �������:
		flag=0;
		do {
				fflush(stdin);
				flag=scanf("%d", &age); // ������ �������
				if (flag == 1 && age>0)
					break;
				else
					printf("\n!---------------------------------!\n! Proshu Vas, bud'te vnimatel'ny! !\n!---------------------------------!\n");
				printf("%d - vozrast: ", i + 1);
			} while (flag!=1 || age<=0);
		if (tempYoung == 0) // ������ ������� ������������ �� ��������� �������� ������������ ��� ������ �������� � ��� ������ �������
		{
			tempYoung = age;
			tempOld = age;
			pYoung = name[i];
			pOld = name[i];
		}
		else if (age < tempYoung) // ���� ��������� ����������� ������ ������ ��������
		{
			tempYoung = age;
			pYoung = name[i]; // ��������� ��������� � ������� ������
		}
		else if (age > tempOld) // ���� ��������� ����������� ������ ������ �������
		{
			tempOld = age;
			pOld = name[i]; // ��������� ��������� � ������� ������
		}
	}
	
	// ������� ������ �������� � ������ ������� �����������
	printf("\nSamyj molodoj rodstvennik: %s - %d\n", pYoung, tempYoung); // ����� ������� �����������:
	printf("Samyj  staryj rodstvennik: %s - %d\n", pOld, tempOld); // ����� ������ �����������:

	return 0;
}