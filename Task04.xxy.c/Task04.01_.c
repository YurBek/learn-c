/*
4.1. �������� ���������, ������� ��������� ������������ ������ ���������
����� � ����������, � ����� ��������� �� � ������� ����������� ����� ������.
���������: ������ �������� �� ��������� ������ ������ � ������������ � ���������
���������� ������. ������������ ���������� �������� � ���������� �������
���������� �� char. ����� ��������� ����� ��������� ��������� �������� �
������� � ������� ������ � ������������ � ���������������� �����������.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

// ������� ������������ ���-�� �������� � ������
#define SYMBOL_MAX 80
// ������� ������������ ���-�� �����
#define LINE_MAX 25

int main()
{
	char string[LINE_MAX][SYMBOL_MAX + 1] = { '\0' };
	char *pString[LINE_MAX] = {NULL}, *temp=NULL;

	int i, j, flag=0, numberLine=0;
	
	printf("Vvedite neskol'ko strok.\nKogda zakonchite nazhmite \"Enter\".\n");
	// ������� ��������� �����. ����� ��������� ������� "Enter".
	i=0;
	do
	{
		gets(string[i]); // ������ ��������� ������
		pString[i] = string[i]; // ��������� ��������� � ������� ������
		//printf("%p - %s - %d\n", pString[i], pString[i], strlen(pString[i]));
		i++;
	} while (strlen(string[i-1])!=0); // ���� ������ ������ - ����������� ����
	numberLine = i-1;

	// ���������� �������� �������� ��� ���������� ���������
	for (i = 0; i<numberLine; i++)
	{
		flag = 0; // ���� ��� �������� �������
		for (j = 0; j<numberLine-i; j++)
		{
			if (strlen(pString[j]) < strlen(pString[j + 1]))
			{
				temp = pString[j];
				pString[j] = pString[j+1];
				pString[j+1] = temp;
				flag = 1; // ���� ����� ��� ����=1
			}
		}
		if (flag == 0) // ���� ������ �� ���� ����=0 - ������� �� �����
			break;
		
	}
	
	// ������� ��������������� ������
	for (i = 0; i<numberLine; i++)
	{
		printf("%s\n", pString[i]);
	}

	return 0;
}