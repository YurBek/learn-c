/*
3.1. �������� ���������, �������������� ���������� ���� �� �������� ������������� ������.
*/

#define _CRT_SECURE_NO_WARNINGS
//#define _CRT_SECURE_NO_DEPRECATE

// ������� ������������ ����� ������
#define LENGTH_MAX 79

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char string[LENGTH_MAX + 1];
	int i, j, counter=0, inWord = 0;

// ������ ������������ ������
	printf("Nazhmite <Enter>\n");
	printf("ili vvedite proizvol'nuju stroku iz neskol'kih slov razdeljaja ih probelami:\n");
	gets(string);

	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������� ������������������ ��������� �������� � ��������
		srand((unsigned)time(NULL));

		string[0] = ' ';
		for (i = 1; i<LENGTH_MAX-1; i++)
		{
			switch (rand() % 3)
			{
			case 0:
				string[i] = ' '; // ������
				break;
			case 1:
				string[i] = 'a' + rand() % ('z' - 'a' + 1); // ������� a-z
				break;
			case 2:
				string[i] = 'A' + rand() % ('Z' - 'A' + 1); // ������� A-Z
				break;
			}
		}
		string[LENGTH_MAX-1] = ' ';
		string[LENGTH_MAX] = '\0';
	}

// ��������� ��� �����, � ����� ��������� ����� �������� ������
	printf("\nDlina ishodnoj stroki - %d\n", strlen(string));
	puts(string);

	i = 0;
	while (string[i])
	{
		if (string[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			counter++;
		}
		else if (string[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	printf("\nVsego slov: %3d\n", counter);

	return 0;
}