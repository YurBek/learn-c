/*
3.9. �������� ���������, ������������ � ������ ����������
������������������ ������������ �����.
���������: ��� ������ AABCCCDDEEEEF ��������� 4 � EEEE.
*/

#define _CRT_SECURE_NO_WARNINGS

// ������� ������������ ����� ������
#define LENGTH_MAX 79

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char string[LENGTH_MAX + 1];
	int i, j, index, positionWord, lengthWord, counterLetter;

	// ������ ������������ ������
	printf("Nazhmite <Enter>\n"); // ������� <Enter>
	printf("ili vvedite stroku iz neskol'kih povtorjajushhihsja simvolov,\nnaprimer: AAABBCDDEEEFFFF\n");
	// ��� ������� ������ �� ���������� ������������� ��������, ��������: AAABBCDDEEEFFFF
	gets(string); // ���� ������������ ������ ������ ������ LENGTH_MAX ��������� ������...

	if (strlen(string) != 0)
		printf("\n");

	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������� ������������������ ��������� �������� ��������� ����: AAABBCDDEEEFFFF
		srand((unsigned)time(NULL));

		for (i = 0; i<LENGTH_MAX; i++)
		{
			counterLetter = 2 + rand() % (LENGTH_MAX - 2);
			for (j = 1; j < counterLetter; j++)
				string[i] = 'A' + rand() % ('C' - 'A' + 1); // ������� A-C
		}
		string[LENGTH_MAX] = '\0';
	}

	// ��������� ��� �����, � ����� ��������� ����� �������� ������
	printf("Ishodnaja stroka:\n"); // �������� ������:
	puts(string);
	printf("\Dlina stroki - %d\n", strlen(string)); // ����� ������ - 

	// ������� ������������������ � �������
	index = 0;
	counterLetter = 0;
	lengthWord = 0;
	positionWord = 0;
	for (i = 0; i < strlen(string); i++)
	{
		if (string[i] != string[i-1]) // ���� ������� ������ �� ����� �����������
		{
			index = i;
			counterLetter=1;
		}
		if (string[i] == string[i - 1]) // ���� ������� ������ ����� �����������
		{
			counterLetter++;
		}
		if (string[i] != string[i + 1]) // ���� ������� ������ �� ����� ������������
		{
			if (lengthWord < counterLetter)
			{
				lengthWord = counterLetter;
				positionWord = index;
			}
		}
	}

	printf("\nPervaja samaja dlinnaja posledovatel'nost' v stroke:\n"); // ������ ����� ������� ������������������ � ������:
	i = positionWord; // ������� ������� ������� ����� ������� ������������������ � ������
	counterLetter = lengthWord;
	while (counterLetter != 0)
	{
		printf("%c", string[i]); // ������� ����������� ������������������
		counterLetter--;
		i++;
	}
	printf(" - %d\n", lengthWord); // ������� ����� ������������������
	
	return 0;
}