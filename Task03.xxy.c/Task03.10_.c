/*
3.10. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n-�� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������.
���������: � ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������!
*/

#define _CRT_SECURE_NO_WARNINGS

// ������� ������������ ����� ������
#define LENGTH_MAX 79

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char string[LENGTH_MAX + 1];
	
	int i, j, format, numberWord, inWord, counterLetter, counterWord, index, lengthWord;

	// ������ ������������ ������
	printf("Nazhmite <Enter>\n"); // ������� <Enter>
	printf("ili vvedite proizvol'nuju stroku iz neskol'kih slov razdeljaja ih probelami:\n");
	// ��� ������� ������������ ������ �� ���������� ���� �������� �� ���������:
	gets(string);
	
	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������ ������ �� ������������������ ��������� �������� � ��������
		srand((unsigned)time(NULL));

		for (i = 0; i<LENGTH_MAX; i++)
		{
			switch (rand() % 3)
			{
			case 0:
				string[i] = ' '; // ������
				break;
			case 1:
				string[i] = 'a' + rand() % ('z' - 'a' + 1); // ������� a-z
				break;
			case 2:
				string[i] = 'A' + rand() % ('Z' - 'A' + 1); // ������� A-Z
				break;
			}
		}
		string[LENGTH_MAX] = '\0';
		puts(string); // ������� ������
	}

	// ������ ���������� ����� ����� ������� ��������� �������
	printf("\nVvedite porjadkovyj nomer slova kotoroe trebuetsja udalit': ");
	do {
		fflush(stdin);
		format = scanf("%d", &numberWord);
		if (format == 1 && numberWord>0)
			break;
		else
		{
			printf("\n!-------------------------------------------------------------------!");
			printf("\n!                 Proshu Vas, bud'te vnimatel'ny!                   !");
			printf("\n!-------------------------------------------------------------------!");
			printf("\nVvedite porjadkovyj nomer slova kotoroe trebuetsja vyvesti na jekran: ");
		}

	} while (format != 1 || numberWord <= 0);

	// ������� ����� � �������
	i = 0;
	inWord = 0;
	counterWord = 0;
	counterLetter = 0;
	while (string[i])
	{
		if (string[i] != ' ' && inWord == 0) // ������� ������ �����, ������ ������ ������
		{
			inWord = 1;
			counterWord++;
			if (counterWord == numberWord)
			{
				index = i;
				counterLetter = 1;
			}
		}
		else if (string[i] != ' ' && inWord == 1) // ���� ����� �������� ������ ������ �������
		{
			if (counterWord == numberWord)
				counterLetter++;
		}
		else if (string[i] == ' ' && inWord == 1) // ������� ����� �����, ������ ������ ����� �� ������
		{
			inWord = 0;
			if (counterWord == numberWord)
				lengthWord = counterLetter;
		}		
		i++;
	}

	// ������� �� ����� ���������
	printf("\nV stroke vsego %3d slov.\n", counterWord); // � ������ ����� _ ����.
	if (counterWord < numberWord)
	{
		printf("\nVy prosili udalit' slovo #%d", numberWord); // �� ������� ������� ����� �
		printf("\nVashego slova v stroke net!\n"); // ������ ����� � ������ ���!
	}
	else
	{
		for (i = 1; i <= lengthWord; i++)
		{
			j = index;
			while (string[j])
			{
				string[j] = string[j + 1];
				j++;
			}
		}
		printf("\nMy udalili slovo #%d\n", numberWord); // �� ������� ����� �_
		puts(string);
	}

	return 0;
}