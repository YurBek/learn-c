/*
3.3. �������� ���������, ������� ��� �������� ������ ������� ����� �������
����� � ��� �����.
*/

#define _CRT_SECURE_NO_WARNINGS

// ������� ������������ ����� ������
#define LENGTH_MAX 79

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char string[LENGTH_MAX + 1];
	char buff[LENGTH_MAX + 1] = { '\0' };
	int i, counterWord, inWord, positionWord, lengthWord, counterLetter;

	// ������ ������������ ������
	printf("Nazhmite <Enter>\n"); // ������� <Enter>
	printf("ili vvedite proizvol'nuju stroku iz neskol'kih slov razdeljaja ih probelami:\n");
	// ��� ������� ������������ ������ �� ���������� ���� �������� �� ���������
	gets(string); // ���� ������������ ������ ������ ������ LENGTH_MAX ��������� ������...

	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������� ������������������ ��������� �������� � ��������
		srand((unsigned)time(NULL));

		for (i = 0; i<LENGTH_MAX; i++)
		{
			switch (rand() % 3)
			{
			case 0:
				string[i] = ' '; // ������
				break;
			case 1:
				string[i] = 'a' + rand() % ('z' - 'a' + 1); // ������� a-z
				break;
			case 2:
				string[i] = 'A' + rand() % ('Z' - 'A' + 1); // ������� A-Z
				break;
			}
		}
		string[LENGTH_MAX] = '\0';
	}

	// ��������� ��� �����, � ����� ��������� ����� �������� ������
	printf("\nDlina ishodnoj stroki - %d\n", strlen(string)); // ����� �������� ������ - 
	puts(string);
	
	// ������� ����� � �������
	inWord = 0;
	counterWord = 0;
	lengthWord = 0;
	i = 0;
	while (string[i])
	{
		if (inWord == 0 && string[i] != ' ') // ������� ������ �����, ������ ������ ������
		{
			inWord = 1;
			counterLetter = 1; // �������� ������� ���������� ���� � �����
			counterWord++;
		}
		else if (inWord == 1 && string[i] != ' ') // ���� ����� �������� ������ ������ �������
		{
			counterLetter++; // ���������� ������� ���������� ���� � �����
		}
		else if (inWord == 1 && string[i] == ' ') // ������� ����� �����, ������ ������ ����� �� ������
		{
			inWord = 0;
			if (lengthWord < counterLetter) // ��������� ����� �����
			{
				lengthWord = counterLetter; // ���������� ����� �����
				positionWord = i - counterLetter; // ��������� � ���������� ������� ������� ������� ����� ����� � ������
			}
		}
		i++;
	}

	// ���� if �������� ���� ������ ����������� �� ��������
	if (lengthWord < counterLetter) // ��������� ����� �����
	{
		lengthWord = counterLetter; // ���������� ����� �����
		positionWord = i - counterLetter; // ��������� � ���������� ������� ������� ������� ����� ����� � ������
	}

	if (counterWord != 0)
	{
		printf("\nVsego slov: %3d\n", counterWord); // ����� ����:
		puts(string);
		printf("\n");

		printf("Samoe dlinnoe slovo v stroke:\n"); // ����� ������� ����� � ������:
		i = positionWord; // ������� ������� ������� ������ �������� ����� � ������, ������� ����������� ������
		counterLetter = lengthWord;
		while (counterLetter != 0)
		{
			printf("%c", string[i]); // ������� ����������� ���� �����
			counterLetter--;
			i++;
		}
		printf(" - %d\n", lengthWord); // ������� ����� �����
	}
	else
	{
		printf("\nVsego slov: %3d\n", counterWord); // ����� ����
		printf("\nSozhaleem, v Vashej stroke slov ne obnaruzheno...\n"); // ���������, � ����� ������ ���� �� ����������...
	}
		
	
	return 0;
}