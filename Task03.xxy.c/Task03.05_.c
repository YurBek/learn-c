/*
3.5. �������� ���������, ������� ��������� ������������� ������ ������� N, �
����� ������� ����� ���������, ������������� ����� ������ ������������� �
��������� ������������� ����������.
���������: ������ ����������� ���������� �������, �������������� � �������������� �������
(��� ����� �������...).
*/

#define _CRT_SECURE_NO_WARNINGS

// ������� ������������ ����� ������
#define ARRAY_MAX 10

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int array[ARRAY_MAX] = {0};
	int i, j;
	int negative = 0;
	int positive = 0;
	int first = 0;
	int last = ARRAY_MAX - 1;
	int sum = 0;
	
	// ��������� ������ ���������� ������� �� -100 �� 100
	srand((unsigned)time(NULL));

	for (i = 0; i<ARRAY_MAX; i++)
		array[i] = -100 + rand() % (100-(-100)) + 1; // �� -100 �� 100
	
	// ������� ������ ��������� �� 10 ���������
	printf("Ishodnyj massiv:\n    "); // �������� ������:
	for (i = 0; i < 10; i++) // ���������� ��������� ��������
		printf("    %02d ", i);
	printf("\n");

	i = 0;
	j = 1;
	while (i < ARRAY_MAX) // ������� ������
	{
		if(j==1)
			printf(" %02d:", (i / 10)*10);
		j != 10 ? (j++ && printf("  %4d ", array[i])) : (j = 1 && printf("  %4d\n", array[i]));
		i++;
	}

	printf("\n");

	// ������ ������ � "������" �������������
	i = 0;
	while (i < ARRAY_MAX) // ���� ��������� - ������ ����������
	{
		if (negative == 0 && array[i] < 0)
		{
			negative = 1;
			printf("Pervyj s \"nachala\" otricatel'nyj jelement - %4d\n", array[i]); // ������ � "������" ������������� �������
			break;
		}
		i++;
	}

	// ������ ������ � "�����" ������������� ��������
	j = ARRAY_MAX - 1;
	while (j > 0) // ���� ��������� - ������ ����������
	{
		if (positive == 0 && array[j] > 0)
		{
			positive = 1;
			printf("Pervyj s \"konca\" polozhitel'nyj jelement - %4d\n", array[j]); // ������ � "�����" ������������� �������
			break;
		}
		j--;
	} 

	// �������� ��������� ���������
	if (j < i) // ���� j �������� ������ i
	{
		first = j;
		last = i;
	}
	else // ���� i ��� � �������� ����� j
	{
		first = i;
		last = j;
	}
		
	// ��������� ����� ���������
	for (i = first+1; i < last; i++)
		sum = sum + array[i];
	printf("Summa jelementov = %4d\n", sum); // ����� ���������
	
	return 0;
}