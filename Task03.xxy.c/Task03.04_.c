/*
3.4. �������� ���������, ������� ������� ����� ����� �� �������� ������.
���������: ��������� ������������� ����������� ������������������ ���� � ������ ���
����� � ������������ �� ��� ������ �����. � ��������� ������������� ����������� ��
������������ ����� ��������, �� ���� ���� ������������ ������ ����� �������
������������������ ����, � ����� ������� �� ��������� �����.
*/

#define _CRT_SECURE_NO_WARNINGS

// ������� ������������ ����� ������
#define LENGTH_MAX 79
// ������� ������������ ���-�� ��������
#define DIGIT_MAX 3

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

int main()
{
	char string[LENGTH_MAX + 1];
	char digit[DIGIT_MAX + 1] = { '\0' };
	int i, j, number;

	// ������ ������������ ������
	printf("Nazhmite <Enter>\n"); // ������� <Enter>
	printf("ili vvedite proizvol'nuju stroku iz neskol'kih slov razdeljaja ih probelami:\n");
	// ��� ������� ������������ ������ �� ���������� ���� �������� �� ���������
	gets(string); // ���� ������������ ������ ������ ������ LENGTH_MAX ��������� ������...

	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������� ������������������ ��������� �������� � ��������
		srand((unsigned)time(NULL));

		for (i = 0; i<LENGTH_MAX; i++)
		{
			switch (rand() % 2)
			{
			case 0:
				string[i] = '0' + rand() % ('9' - '0' + 1); // ����� 0-9
				break;
			case 1:
				string[i] = 'a' + rand() % ('z' - 'a' + 1); // ������� a-z
				break;
			/*case 2:
				string[i] = 'A' + rand() % ('Z' - 'A' + 1); // ������� A-Z
				break;
			case 3:
				string[i] = ' '; // ������
				break;*/
			}
		}
		string[LENGTH_MAX] = '\0';
	}

	// ��������� ��� �����, � ����� ��������� ����� �������� ������
	printf("\nDlina ishodnoj stroki - %d\n", strlen(string)); // ����� �������� ������ - 
	puts(string);
	
	// ������� �����, ���������� � �������
	printf("\nSumma chisel:\n"); // ����� �����
	number = 0;
	i = 0;
	j = 0;
	while (string[i])
	{
		if (isdigit(string[i]) && j<DIGIT_MAX) // ������� �����
		{
			digit[j] = string[i];
			digit[j + 1] = '\0';
			printf("%c", digit[j]); // ����������� ������� ����� �� �����
			if (!(isdigit(string[i + 1]))) // ���� ����� �� ������ ���� "�� �����"
			{
				printf(" + ");
				number = number + atoi(digit); // ������������� �����
			}
			j++;
		}
		else if (isdigit(string[i])) // ���� ����������� ��� �������
		{
			printf(" + ");
			number = number + atoi(digit); // ������������� �����
			j = 0;
			i--;
		}
		else
			j = 0;
		i++;
	}

	printf(" = %6d\n", number);
	
	return 0;
}