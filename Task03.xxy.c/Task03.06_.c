/*
3.6. �������� ���������, ������� ��������� ������������� ������ ������� N,
� ����� ������� ����� ��������� ����� ����������� � ������������ ����������.
*/

#define _CRT_SECURE_NO_WARNINGS

// ������� ������������ ������ �������
#define ARRAY_MAX 10

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int array[ARRAY_MAX] = {0};
	int i, j;
	int temp;
	int min = 0;
	int max = 0;
	int first = 0;
	int last = ARRAY_MAX - 1;
	int sum = 0;
	
	// ��������� ������ ���������� ������� �� -100 �� 100
	srand((unsigned)time(NULL));

	for (i = 0; i<ARRAY_MAX; i++)
		array[i] = -100 + rand() % (100-(-100)) + 1; // �� -100 �� 100
	
	// ������� ������ ��������� �� 10 ���������
	printf("Ishodnyj massiv:\n    "); // �������� ������:
	for (i = 0; i < 10; i++) // ���������� ��������� ��������
		printf("    %02d ", i);
	printf("\n");

	i = 0;
	j = 1;
	while (i < ARRAY_MAX) // ������� ������
	{
		if(j==1)
			printf(" %02d:", (i / 10)*10);
		j != 10 ? (j++ && printf("  %4d ", array[i])) : (j = 1 && printf("  %4d\n", array[i]));
		i++;
	}
	printf("\n");

	// ������ ������ � "������" ����������� �������
	i = 0;
	temp = 0;
	while (i < ARRAY_MAX)
	{
		if (array[i] < temp)
		{
			min = i;
			temp = array[i];
		}
		i++;
	}
	printf("Pervyj s \"nachala\" minimal'nyj jelement - %4d\n", temp); // ������ � "������" ����������� �������

	// ������ ������ � "�����" ������������ �������
	j = ARRAY_MAX - 1;
	temp = 0;
	while (j > 0)
	{
		if (array[j] > temp)
		{
			max = j;
			temp = array[j];
		}
		j--;
	} 
	printf("Pervyj s \"konca\" maksimal'nyj jelement - %4d\n", temp); // ������ � "�����" ������������ �������

	// �������� ��������� ���������
	if (max < min) // ���� min �������� ������ max
	{
		first = max;
		last = min;
	}
	else // ���� min ��� � �������� ����� max
	{
		first = min;
		last = max;
	}
		
	// ��������� ����� ���������
	for (i = first+1; i < last; i++)
		sum = sum + array[i];
	printf("Summa jelementov = %4d\n", sum); // ����� ���������
	
	return 0;
}