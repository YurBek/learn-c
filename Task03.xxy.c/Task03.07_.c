/*
3.7. �������� ���������, ������� �������� ������� ������������� �������� ���
�������� ������, ��������������� �� �������� �������.
���������: ������� ��������� ����� �������, ��� ������� ���� ����� ��������������
�������, � ����� �� ���� ��������.
*/

#define _CRT_SECURE_NO_WARNINGS
//#define _CRT_SECURE_NO_DEPRECATE

// ������� ������������ ����� ������
#define LENGTH_MAX 80
// ������� ������������ ���������� �������� � "�������"
#define SIMBOL_MAX 95

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char string[LENGTH_MAX + 1];
	char symbol[SIMBOL_MAX] = { '\0' };
	char tempSymbol = '\0';

	int frequency[SIMBOL_MAX] = { 0 };
	int tempFrequency = 0;
	int i, j, flag, lengthString;
	
	// ������ ������������ ������
	printf("Nazhmite <Enter>\n"); // ������� <Enter>
	printf("ili vvedite proizvol'nuju stroku\nispol'zuja simvoly, cifry i probely:\n");
	// ��� ������� ������������ ������ ��������� �������, ����� � �������:
	gets(string);

	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������� ������������������ ��������� �������� � ��������
		srand((unsigned)time(NULL));

		for (i = 0; i<LENGTH_MAX; i++)
		{
			string[i] = 'a' + rand() % ('j' - 'a' + 1); // ������� �� a �� j
			//string[i] = '0' + rand() % ('z' - '0' + 1); // ������� �� 0 �� z
			//string[i] = '!' + rand() % ('~' - '!' + 1); // ������� !-~, ����� a-z � A-Z, ����� 0-9
			/*switch (rand() % 2)
			{
			case 0:
				string[i] = ' '; // ������
				break;
			case 1:
				string[i] = '!' + rand() % ('~' - '!' + 1); // ������� !-~, ����� a-z � A-Z, ����� 0-9
				break;
			}*/
		}
		string[LENGTH_MAX] = '\0';
	}

	// ��������� ��� �����, � ����� ��������� ����� �������� ������
	printf("\nDlina ishodnoj stroki - %d\n", strlen(string)); // ����� �������� ������ - 
	puts(string);
	
	// �������� "�������" ����������� ��������
	for (i=0, lengthString = strlen(string)+1; i < lengthString; i++)
	{
		for (j=0; j<SIMBOL_MAX; j++)
		{
			// ���� ������ ��� ���� � ������� ������� �� �����
			if (symbol[j] == string[i])
				break;
			// ���� ��������� '\0' ������ ������� � "�������" ��� ���
			// ��������� � "�������" � ������� �� �����
			else if (symbol[j] == '\0')
			{
				symbol[j] = string[i];
				break;
			}
		}
	}
	
	// ������� "�������"
	printf("\nSlovar' stroki:\n"); // ������� ������:
	puts(symbol);
	
	// ���������� ������� ������������� ��������
	printf("\nnot sorted\n"); // �� �������������
	printf("CHAR  FREQ\n"); // ���� ����
	for (i = 0; i < SIMBOL_MAX; i++)
	{
		if (symbol[i] == '\0')
			break;
		for (j = 0; j < lengthString; j++)
		{
			if (symbol[i] == string[j])
			{
				frequency[i] += 1;
			}
		}
		printf("  %c   %3d\n", symbol[i], frequency[i]);
	}

	// ����������� ������� �� �������� �������
	// ���������� �������� �������� ��� ���������� ���������
	for (i = 0; i < SIMBOL_MAX; i++)
	{
		flag = 0; // ���� ��� �������� �������
		for (j = 0; j < SIMBOL_MAX - i; j++)
		{
			if (frequency[j] < frequency[j + 1]) // ���������� ������� �������� �� ���������
			{
				// ���������� �������
				tempFrequency = frequency[j];
				frequency[j] = frequency[j + 1];
				frequency[j + 1] = tempFrequency;
				// ���������� �������
				tempSymbol = symbol[j];
				symbol[j] = symbol[j + 1];
				symbol[j + 1] = tempSymbol;
				// ���� ����� ��� ����=1
				flag = 1;
			}
		}
		if (flag == 0) // ���� ������ �� ���� ����=0 - ������� �� �����
			break;
	}

	// ������� �� ����� ��������������� �� �������� ������� ������� ��������
	printf("\n sorted\n"); // ���� ���� �������������
	printf("CHAR FREQ\n"); // ���� ���� �������������
	i = 0;
	while (frequency[i] != 0)
	{
		printf("  %c   %3d\n", symbol[i], frequency[i]);
		i++;
	}

	return 0;
}