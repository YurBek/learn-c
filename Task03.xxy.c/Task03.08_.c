/*
3.8. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n-�� ����� ������ �� �����.
� ������ ������������� n ��������� ��������� �� ������.
*/

#define _CRT_SECURE_NO_WARNINGS

// ������� ������������ ����� ������
#define LENGTH_MAX 79

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char string[LENGTH_MAX + 1];
	char buff[LENGTH_MAX + 1] = { '\0' };

	int i, j, counter, inWord, numberWord, format;

	// ������ ������������ ������
	printf("Nazhmite <Enter>\n"); // ������� <Enter>
	printf("ili vvedite proizvol'nuju stroku iz neskol'kih slov razdeljaja ih probelami:\n");
	// ��� ������� ������������ ������ �� ���������� ���� �������� �� ���������:
	gets(string);

	// ������ ���������� ����� ����� ������� ��������� ������� �� �����
	printf("\nVvedite porjadkovyj nomer slova kotoroe trebuetsja vyvesti na jekran: ");
	do {
		fflush(stdin);
		format = scanf("%d", &numberWord);
		if (format == 1 && numberWord>0)
			break;
		else
		{
			printf("\n!-------------------------------------------------------------------!");
			printf("\n!                 Proshu Vas, bud'te vnimatel'ny!                   !");
			printf("\n!-------------------------------------------------------------------!");
			printf("\nVvedite porjadkovyj nomer slova kotoroe trebuetsja vyvesti na jekran: ");
		}

	} while (format != 1 || numberWord<=0);

	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������ ������ �� ������������������ ��������� �������� � ��������
		srand((unsigned)time(NULL));

		for (i = 0; i<LENGTH_MAX; i++)
		{
			switch (rand() % 3)
			{
			case 0:
				string[i] = ' '; // ������
				break;
			case 1:
				string[i] = 'a' + rand() % ('z' - 'a' + 1); // ������� a-z
				break;
			case 2:
				string[i] = 'A' + rand() % ('Z' - 'A' + 1); // ������� A-Z
				break;
			}
		}
		string[LENGTH_MAX] = '\0';
	}

	// ������� ����� � �������
	counter = 0;
	inWord = 0;
	i = 0;
	while (string[i])
	{
		if (string[i] != ' ' && inWord == 0) // ������� ������ �����, ������ ������ ������
		{
			j = 0;
			inWord = 1;
			counter++;
			if (counter == numberWord) // �������� ������ ������ ����� ����� �������� �� ����� � ������
			{
				buff[j]=string[i];
				buff[j + 1] = '\0';
			}
		}
		else if (string[i] != ' ' && inWord == 1) // ���� ����� �������� ������ ������ �������
		{
			j++;
			if (counter == numberWord) // �������� ��������� ������� ����� ����� �������� �� ����� � ������
			{
				buff[j] = string[i];
				buff[j + 1] = '\0';
			}
		}
		else if (string[i] == ' ' && inWord == 1) // ������� ����� �����, ������ ������ ����� �� ������
		{
			inWord = 0;
		}
		i++;
	}
	
	// ������� �� ����� ���������
	printf("\nV stroke:\n"); // � ������:
	puts(string);
	printf("\nvsego slov: %3d\n", counter); // ����� ����:
	if (numberWord <= counter)
	{
		printf("\nVy prosili slovo #%d", numberWord); // �� ������� ����� �
		printf("\n%s\n", buff);
	}
	else
	{
		printf("\nVy prosili slovo #%d", numberWord); // �� ������� ����� �
		printf("\nVashego slova zdes' net!\n"); // ������ ����� ����� ���!
	}

	return 0;
}