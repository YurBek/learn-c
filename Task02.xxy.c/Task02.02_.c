/*
2.2. �������� ��������� ������� �����.
��������� ���������� ����� � ��������� �� 1 �� 100 � ������������ ������ ������� ���
�� ���������� ���������� �������.
���������: ������������ ������ �����, � ��������� ������������ ���: �������, �������, �������!�.
*/

/*
� ���� ������ ��������� "������ �����" ��������� ������� ��������� � ���� ����: "�������", "�����",
"������", "������" � "��������".
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int number, try=1, format;

// ������� ��������� ����� �� 1 �� 100
	srand((unsigned)time(NULL));
	int numberHidden = rand() % 100 + 1;

// ��������� ���������� �����
//	printf("Zagadannoe chislo: %d\n", numberHidden);

// ��������� �������� ������������� �����
	printf("\nKomp'juter zagadal chislo ot 1 do 100.\n");
	printf("Poprobujte ego ugadat'! Vvedite svoe chislo: ");
	do{
		fflush(stdin);
		format=scanf("%d", &number);
		if (format == 1)
		{
			if (number == numberHidden) // ���� ������!!!
			{
				printf("\nOtlichno, Vy ugadali!!!\n");
				printf("Popytok bylo: %d\n", try);
				continue;
			}
			else if (number == numberHidden - 1 || number == numberHidden + 1) // ���� ��������!!!
			{
				try++;
				printf("\nUh! Obzhigaet!!! Vvedite svoe chislo:        ");
				continue;
			}
			else if ((number >= numberHidden - 5 && number < numberHidden - 1) || (number > numberHidden + 1 && number <= numberHidden + 5)) // ���� ������!
			{
				try++;
				printf("\nGorjacho! Vvedite svoe chislo:               ");
				continue;
			}
			else if ((number >= numberHidden - 15 && number < numberHidden - 5) || (number > numberHidden + 5 && number <= numberHidden + 15)) // ���� ������!
			{
				try++;
				printf("\nTeplee! Vvedite svoe chislo:                 ");
				continue;
			}
			else if ((number >= numberHidden - 30 && number < numberHidden - 15) || (number > numberHidden + 15 && number <= numberHidden + 30)) // ���� �����!
			{
				try++;
				printf("\nTeplo! Vvedite svoe chislo:                  ");
				continue;
			}
			else // ���� �������...
			{
				try++;
				printf("\nHolodno... Vvedite svoe chislo:              ");
			}
		}	
		else
		{
			printf("\n!---------------------------------!\n! Proshu Vas, bud'te vnimatel'ny! !\n!---------------------------------!\n");
			printf("Vvedite svoe chislo:                           ");
		}
	} while (format != 1 || number != numberHidden);
	
	return 0;
}