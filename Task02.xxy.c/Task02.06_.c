/*
2.6. �������� ���������, ��������� ������ �� ������ ��������.
������� ��������� ������� � ������ ������, � ����� ������
� ������� ����� �������, ���� �� ���������� ������ 1.
���������: � ������ ��������� ��������� ��������� �������������� �������,
�� ���� ���������� ���������� � �������� ������. ����� ���������� ���������
�������� �� �����.
*/

#define _CRT_SECURE_NO_WARNINGS
//#define _CRT_SECURE_NO_DEPRECATE

// ������� ������������ ����� ������
#define LENGTH_MAX 80

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char string[LENGTH_MAX+1];
	int i, j, lengthString;

// ������ ������������ ������
	printf("Nazhmite <Enter>\n");
	printf("ili vvedite proizvol'nuju stroku\nispol'zuja simvoly, cifry i probely:\n");
	gets(string);

	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������� ������������������ ��������� �������� (a-z, A-Z) � ��������
		srand((unsigned)time(NULL));

		string[0] = ' ';
		for (j = 1; j<LENGTH_MAX-1; j++)
		{
			switch (rand() % 4)
			{
			case 0:
				string[j] = ' '; // ������
				break;
			case 1:
				string[j] = '  '; // ������� ������
				break;
			case 2:
				string[j] = 'a' + rand() % ('z' - 'a' + 1); // ������� a-z
				break;
			case 3:
				string[j] = 'A' + rand() % ('Z' - 'A' + 1); // ������� A-Z
				break;
			}
		}
		string[LENGTH_MAX-1] = ' ';
		string[LENGTH_MAX] = '\0';
	}

// ��������� ��� �����, � ����� ��������� ����� �������� ������
	printf("\nDlina ishodnoj stroki - %d\n", strlen(string));
	puts(string);

// "������" ������ �� "������" ��������
	for (i = 0, lengthString = strlen(string) + 1; i < lengthString; i++)
	{
		if ((i == 0 && string[i] == ' ') || (string[i] == ' ' && string[i + 1] == ' ') || (i == lengthString - 2 && string[i] == ' '))
		{
			for (j = i; j < lengthString - 1; j++)
				string[j] = string[j + 1];
			lengthString--;
			i--;
		}
	}

// ������� ��������� � ����� ������
	printf("\nDlina chistoj stroki - %d\n", strlen(string));
	puts(string);

	return 0;
}