/*
2.5. �������� ���������, ������� ������� �� ����� 10 �������,
��������������� ��������� ������� �� ��������� ���� � ����,
������ ����� ������ ���� ��� � ������, ��� � � ������� ���������.
����� ������ - 8 ��������.
���������: ������ ���������������� ������: Nh1ku83k.
*/

#define _CRT_SECURE_NO_WARNINGS

// ������� ������������ ����� ������
#define LENGTH_PASSWORD 8
// ������� ���������� �������
#define QUANTITY 10

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char password[LENGTH_PASSWORD+1];
	int i, j;
	srand((unsigned)time(NULL));
	
	printf("###   PSSWORD\n\n");

	for (i = 1; i<=QUANTITY; i++)
	{
		for (j = 0; j<LENGTH_PASSWORD; j++)
		{
			switch(rand()%3)
			{
			case 0:
				password[j] = '0' + rand() % ('9' - '0' + 1); //%10
				break;
			case 1:
				password[j] = 'A' + rand() % ('Z' - 'A' + 1); //%26
				break;
			case 2:
				password[j] = 'a' + rand() % ('z' - 'a' + 1); //%26
				break;
			}
		}
		password[LENGTH_PASSWORD] = '\0';
	
		printf("%03d - %s\n\n", i, password);
	}
	return 0;
}