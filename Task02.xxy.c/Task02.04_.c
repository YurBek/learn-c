/*
2.4. �������� ���������, �������������� ������� � ������� �������� �������:
������� ���� ��������� �����, ����� �����.
������ �������� � ���� ��������� � ���� ��������� ������������������ ���� � ����.
������������ ��������������� ��������� ������.
���������: ���������� � ������ ������ ����������� ����� �� ������������.
����� ������������ ����������� ��������� �������.
*/

/*
������� �������� ��� ������ - ����������� ������� � ������ �� ��. ��������: �����������������0123456789
*/
#define _CRT_SECURE_NO_WARNINGS

// ������� ������������ ����� ������
#define LENGTH_MAX 80

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char string[LENGTH_MAX], temp;
	int i, j, lengthString;
	
// ������� ������������������ ��������� �������� (a-z, A-Z) � ���� (0-9)
	srand((unsigned)time(NULL));
	for (j = 0; j<LENGTH_MAX-1; j++)
	{
		switch(rand() % 3)
		{
		case 0:
			string[j] = '0' + rand() % ('9' - '0' + 1); //%10
			break;
		case 1:
			string[j] = 'A' + rand() % ('Z' - 'A' + 1); //%26
			break;
		case 2:
			string[j] = 'a' + rand() % ('z' - 'a' + 1); //%26
			break;
		}
	}
	string[LENGTH_MAX-1] = '\0';
	
// ������� �������� ������
	printf("%s\n\n", string);

// ��� ������ ��������� ������ ��� ��������� � 0123456789
	for (j = 0, lengthString = LENGTH_MAX - 1; (j != lengthString && j<lengthString); j++)
	{
		if (string[j] <= 'Z')
		{
			temp = string[j];
			for (i = j; i < LENGTH_MAX - 2; i++)
				string[i] = string[i + 1];
			string[LENGTH_MAX - 2] = temp;
			lengthString--;
			j--;
		}
	}

// ������� ���������
	printf("%s\n\n", string);

// ������ �� ����� ��������� � 01234567890 ��������� ������ ������ 01234567890
	for (lengthString = LENGTH_MAX - 1; (j != lengthString && j<lengthString); j++)
	{
		if (string[j] <= '9')
		{
			temp = string[j];
			for (i = j; i < LENGTH_MAX - 2; i++)
				string[i] = string[i + 1];
			string[LENGTH_MAX - 2] = temp;
			lengthString--;
			j--;
		}
	}

// ������� ���������
	printf("%s\n\n", string);

	return 0;
}