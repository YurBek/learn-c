/*
2.3.  �������� ���������, ��������� �� ����� ����������� �� ��������.
���������: ����������� ������ ��������� ���:
  *
 ***
*****
���������� ����� ������� ������������� � ����������.
*/

#define _CRT_SECURE_NO_WARNINGS

/*
��� ����������� ������ ������ ������ � 80 ��������, ������������ ������ ������������
�������� 40 �����, � ������ ��������� 79 �������� + ������ '\0'.
*/
#define CHAR_MAX 80
#define LINE_MAX 40

#include <stdio.h>

int main()
{
	int lines, chars, i, j, k, format;
	char temp;
		
// ����������� ���������� ����� � ��������� �������� ������
	printf("Vvedite chislo strok: ");
	do{
		fflush(stdin);
		format = scanf("%d", &lines);
		if (format == 1 && lines > 0 && lines <= 40)
			break;
		else
		{
			printf("\n!---------------------------------!\n!Proshu Vas, bud'te vnimatel'ny!  !\n!---------------------------------!\n");
			printf("\nVvedite chislo strok: ");
		}
	} while (format != 1 || lines <= 0 || lines > 40);
		
// ��� ���������� �������� ����� ��������� ������������ ������ ��������� ������������ � ��������
	if (lines == 1)
		chars = 1;
	else
		chars = 2 * lines - 1;
	printf("\nLines= %4d", lines);
	printf("; Chars= %4d", chars);
	printf("\n\n");
	
// ������ 1: ������ ����������� ������ ����� ����, �� �������, �� ���������...
	for (i = 1; i <= lines; i++)
	{
		for (j = 1; j <= lines - i; j++)
			printf("%c", ' ');
		for (j = 1; j <= 2 * i - 1; j++)
			printf("%c", '*');
		printf("\n");
	}

// ������ 2: ��������� ���������� ������.
	char furTree[CHAR_MAX];

// ������ ��������� ���������� ���������.
	srand((unsigned)time(NULL));
	for (i = 1; i <= lines; i++)
	{
		for (k = 0; k < chars; k++)
			switch (rand() % 3)
		{
			case 0:
				furTree[k] = '0' + rand() % ('9' - '0' + 1); //%10
				break;
			case 1:
				furTree[k] = 'A' + rand() % ('Z' - 'A' + 1); //%26
				break;
			case 2:
				furTree[k] = 'a' + rand() % ('z' - 'a' + 1); //%26
				break;
		}

		for (k; k < CHAR_MAX - 1; k++)
			furTree[k]=' ';
		furTree[CHAR_MAX - 1] = '\0';

// ����� � ������ � ������ ������� ����������� ���������.
		for (j = lines - i; j <= i + (lines - 2); j++)
		{
			if (i % 10 == 0 && j == lines - 1)
// ���� ������ ����� �� ����� 10, 20, 30 � 40, �� � ������ ������� ������ "#".
				furTree[j] = '#';
			else
				furTree[j] = '*';
		}
// � ������ ��������� �� �����.
		puts(furTree);
	}
		
	return 0;
}