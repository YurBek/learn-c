/*
2.7. �������� ���������, ��������� ������� ������������� �������� ��� ��������� ������������� ������.
� ���� ������� ���������� ������ ������ � ����� ��� ����������.
���������: � ���� ��������� �� ��������� � �������� �������, ��� ��� �������������
�������������� �������� ���������.
*/

#define _CRT_SECURE_NO_WARNINGS
//#define _CRT_SECURE_NO_DEPRECATE

// ������� ������������ ����� ������
#define LENGTH_MAX 80
// ������� ������������ ���������� ��������
#define SIMBOL_MAX 95

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	char string[LENGTH_MAX + 1], symbol[SIMBOL_MAX] = {'\0'};
	int i, j, lengthString, counter;

// ������ ������������ ������
	printf("Nazhmite <Enter>\n");
	printf("ili vvedite proizvol'nuju stroku\nispol'zuja simvoly, cifry i probely:\n");
	gets(string);

	if (strlen(string) == 0) //���� ���� ������� ������ �������
	{
		// ������� ������������������ ��������� �������� � ��������
		srand((unsigned)time(NULL));

		for (i = 0; i<LENGTH_MAX; i++)
		{
			//string[i] = 'a' + rand() % ('c' - 'a' + 1); // ������� �� a �� c
			//string[i] = '0' + rand() % ('z' - '0' + 1); // ������� �� 0 �� z
			string[i] = '!' + rand() % ('~' - '!' + 1); // ������� !-~, ����� a-z � A-Z, ����� 0-9
			/*switch (rand() % 2)
			{
			case 0:
				string[i] = ' '; // ������
				break;
			case 1:
				string[i] = '!' + rand() % ('~' - '!' + 1); // ������� !-~, ����� a-z � A-Z, ����� 0-9
				break;
			}*/
		}
		string[LENGTH_MAX] = '\0';
	}

// ��������� ��� �����, � ����� ��������� ����� �������� ������
	printf("\nDlina ishodnoj stroki - %d\n", strlen(string));
	puts(string);
	
// �������� "��������"
	for (i=0, lengthString = strlen(string)+1; i < lengthString; i++)
	{
		for (j=0; j<SIMBOL_MAX; j++)
		{
			if (symbol[j] == string[i])
				break;
			else if (symbol[j] == '\0')
			{
				symbol[j] = string[i];
				break;
			}
		}
	}
	
// ������� "��������"
	printf("Slovar' stroki:\n");
	puts(symbol);
	
// ���������� ������� ��������
	printf("\nCHAR FREQ\n");
	for (i = 0; i < SIMBOL_MAX; i++)
	{
		if (symbol[i] == '\0')
			break;
		for (j = 0, counter=0; j < lengthString; j++)
		{
			if (symbol[i] == string[j])
				counter++;
		}
		printf("  %c   %03d\n", symbol[i], counter);
	}

	return 0;
}